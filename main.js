$(document).ready(function () {
    //build tree
    function BuildVerticaLTree(treeData, treeContainerDom) {
        var margin = { top: 0, right: 120, bottom: 25, left: 120 };
        var width = window.innerWidth - margin.right - margin.left;
        var height = window.innerHeight - margin.top - margin.bottom;

        var i = 0, duration = 750;
        var tree = d3.layout.tree()
            .size([height, width]);
        var diagonal = d3.svg.diagonal()
            .projection(function (d) { return [d.y, d.x]; });
        var svg = d3.select(treeContainerDom).append("svg")
            .attr("width", width + margin.right + margin.left)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        root = treeData;

        update(root);
        function update(source) {
            // Compute the new tree layout.
            var nodes = tree.nodes(root).reverse(),
                links = tree.links(nodes);
            // Normalize for fixed-depth.
            nodes.forEach(
                function (d) {
                    d.y = 30 + (d.depth * 140);
                }
            );
            // Declare the nodes…
            var node = svg.selectAll("g.node")
                .data(nodes, function (d) { return d.id || (d.id = ++i); });
            // Enter the nodes.
            var nodeEnter = node.enter().append("g")
                .attr("class", "node")
                .attr("transform", function (d) {
                    return "translate(" + source.x0 + "," + source.y0 + ")";
                }).on("click", nodeclick);
            nodeEnter.append("circle")
                .attr("r", 10)
                .attr("stroke", function (d) { return d.children || d._children ? "steelblue" : "#00c13f"; })
                .style("fill", function (d) { return d.children || d._children ? "lightsteelblue" : "#fff"; });
            //.attr("r", 10)
            //.style("fill", "#fff");
            nodeEnter.append("text")
                .attr("x", function (d) {
                    return -60 - (d.name.length)
                })
                // .attr("y", function (d) {
                //     return d.children || d._children ? -20 : 20;
                // })
                // .attr("dy", ".5em")
                .attr("text-anchor", "middle")
                .text(function (d) { return d.name; })
                .style("fill-opacity", 1e-6);
            // Transition nodes to their new position.
            //horizontal tree
            var nodeUpdate = node.transition()
                .duration(duration)
                .attr("transform", function (d) { return "translate(" + d.y + "," + d.x + ")"; });
            nodeUpdate.select("circle")
                .attr("r", 10)
                .style("fill", function (d) { return d._children ? "lightsteelblue" : "#fff"; });
            nodeUpdate.select("text")
                .style("fill-opacity", 1);


            // Transition exiting nodes to the parent's new position.
            var nodeExit = node.exit().transition()
                .duration(duration)
                .attr("transform", function (d) { return "translate(" + source.y + "," + source.x + ")"; })
                .remove();
            nodeExit.select("circle")
                .attr("r", 1e-6);
            nodeExit.select("text")
                .style("fill-opacity", 1e-6);
            // Update the links…
            // Declare the links…
            var link = svg.selectAll("path.link")
                .data(links, function (d) { return d.target.id; });
            // Enter the links.
            link.enter().insert("path", "g")
                .attr("class", "link")
                .attr("d", function (d) {
                    var o = { x: source.y0, y: source.x0 };
                    return diagonal({ source: o, target: o });
                });
            // Transition links to their new position.
            link.transition()
                .duration(duration)
                .attr("d", diagonal);


            // Transition exiting nodes to the parent's new position.
            link.exit().transition()
                .duration(duration)
                .attr("d", function (d) {
                    var o = { x: source.x, y: source.y };
                    return diagonal({ source: o, target: o });
                })
                .remove();

            // Stash the old positions for transition.
            nodes.forEach(function (d) {
                d.x0 = d.y;
                d.y0 = d.x;
                if (d.depth == 3 && d.children != undefined && source.name != d.name && !d.children.includes(source)) {
                    d._children = d.children;
                    d.children = null;
                    update(d)
                }
            });
        }

        // Toggle children on click.
        function nodeclick(d) {
            if (d.depth == 3) {
                if (d.children) {
                    d._children = d.children;
                    d.children = null;
                    selectLeaf({ title: "", sprint: "", microSprint: "" })
                } else {
                    if (d.depth == 3) {
                        parent = d.parent.parent.parent
                        childrenn = []
                        children = parent.children || []
                        for (i = 0; i < children.length; i++) {
                            childreen = children[i].children || []
                            for (j = 0; j < childreen.length; j++) {
                                childreeen = childreen[j].children || []
                                for (k = 0; k < childreeen.length; k++) {
                                    dd = childreeen[k]
                                    if (dd.name != d.name && dd.children != undefined) {
                                        dd._children = dd.children;
                                        dd.children = null;
                                        update(dd)
                                    }
                                }
                            }
                        }
                    }
                    d.children = d._children;
                    d._children = null;
                }
            }
            if (d.depth == 1) {
                selectLeaf({ gidea: d.description, opedg: "", eperd: "", title: "", sprint: "", microSprint: "" })
            } else if (d.depth == 2) {
                selectLeaf({ gidea: d.parent.description, opedg: d.description, eperd: "", title: "", sprint: "", microSprint: "" })
            } else if (d.depth == 3) {
                selectLeaf({ gidea: d.parent.parent.description, opedg: d.parent.description, eperd: d.description, title: "", sprint: "", microSprint: "" })
            } else {
                selectLeaf({ gidea: d.parent.parent.parent.description, opedg: d.parent.parent.description, eperd: d.parent.description, title: d.data.title, sprint: d.data.sprint, microSprint: d.data.microSprint })
            }
            update(d);
        }
    }


    d3.json("main.json", function (data) {
        BuildVerticaLTree(data, "#tree");
    })

    function selectLeaf(val) {
        $("#gidea").text(val.gidea);
        $("#opedg").text(val.opedg);
        $("#eperd").text(val.eperd);
        $("#title").text(val.title);
        $("#sprint").text(val.sprint);
        $("#microSprint").text(val.microSprint)
    }
});